//
//  SelectDestinationViewController.swift
//  InspireRide
//
//  Created by John Yang on 2/21/16.
//  Copyright © 2016 John Yang. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import UberRides

class SelectDestinationViewController: UIViewController,CLLocationManagerDelegate, MKMapViewDelegate {

    var startLongitude : Double = 0
    var startLatitude: Double = 0
    var destinationLongitude : Double = 0
    var destinationLatitude: Double = 0
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var pickupView: UIView!
    let locationManager = CLLocationManager()
    @IBOutlet weak var requestButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        self.map.showsUserLocation = true
        
        self.segmentedControl.setTitle("Peace", forSegmentAtIndex: 1)
        self.segmentedControl.setTitle("Energy", forSegmentAtIndex: 0)
        self.segmentedControl.insertSegmentWithTitle("Focus", atIndex: 2, animated: false)
        self.segmentedControl.tintColor = UIColor.whiteColor()
        pickupView.backgroundColor = UIColor(red: 26/255, green: 26/255, blue: 26/255, alpha: 0.8)
    }
    
    @IBAction func requestButtonClicked(sender: AnyObject) {
        let url = NSURL(string: "uber://?action=setPickup&pickup[latitude]=\(startLatitude)&pickup[longitude]=\(startLongitude)&dropoff[latitutde]=\(destinationLatitude)&dropoff[longitude]=\(destinationLongitude)")
        UIApplication.sharedApplication().openURL(url!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span:MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
        self.map.setRegion(region, animated: true)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Errors: \(error.localizedDescription)")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension SelectDestinationViewController {
    
    @IBAction func segmentedControlClicked(sender: AnyObject) {
    }
    
    @IBAction func segmentedControlTouched(sender: AnyObject) {
        //Set Start Longitude and Latitude
        if segmentedControl.selectedSegmentIndex == 0 {
            let annotationsToRemove = map.annotations.filter { $0 !== map.userLocation }
            map.removeAnnotations(annotationsToRemove)
            
            let dropPin = MKPointAnnotation()
            dropPin.coordinate = CLLocationCoordinate2DMake(37.399040,  -122.015983)
            dropPin.title = "Seven Seas Park"
            map.addAnnotation(dropPin)
            
            let dropPin1 = MKPointAnnotation()
            dropPin1.coordinate = CLLocationCoordinate2DMake(37.431943, -122.087041)
            dropPin1.title = "Shoreline Park at Mountain View"
            map.addAnnotation(dropPin1)
            
            let dropPin2 = MKPointAnnotation()
            dropPin2.coordinate = CLLocationCoordinate2DMake(37.413140, -121.998879)
            dropPin2.title = "Baylands Park"
            map.addAnnotation(dropPin2)
        }
        if segmentedControl.selectedSegmentIndex == 1 {
            let annotationsToRemove = map.annotations.filter { $0 !== map.userLocation }
            map.removeAnnotations(annotationsToRemove)
            
            let dropPin = MKPointAnnotation()
            dropPin.coordinate = CLLocationCoordinate2DMake(37.403506, -122.008602)
            dropPin.title = "FTX Martial Arts and Fitness"
            map.addAnnotation(dropPin)
            
            let dropPin1 = MKPointAnnotation()
            dropPin1.coordinate = CLLocationCoordinate2DMake(37.476459, -122.203283)
            dropPin1.title = "SportsHouse"
            map.addAnnotation(dropPin1)
            startLatitude = 10
            startLongitude = 10
        }
        if segmentedControl.selectedSegmentIndex == 2{
            let annotationsToRemove = map.annotations.filter { $0 !== map.userLocation }
            map.removeAnnotations(annotationsToRemove)

            let dropPin = MKPointAnnotation()
            dropPin.coordinate = CLLocationCoordinate2DMake(37.414234, -122.077362)
            dropPin.title = "Computer History Museum"
            map.addAnnotation(dropPin)
            
            let dropPin1 = MKPointAnnotation()
            dropPin1.coordinate = CLLocationCoordinate2DMake(37.331629, -121.890234)
            dropPin1.title = "Tech Museum of Innovation"
            map.addAnnotation(dropPin1)
        }
    }
}
