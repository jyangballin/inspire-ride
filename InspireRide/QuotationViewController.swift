//
//  QuotationViewController.swift
//  InspireRide
//
//  Created by John Yang on 2/21/16.
//  Copyright © 2016 John Yang. All rights reserved.
//

import UIKit

class QuotationViewController: UIViewController {

    @IBOutlet weak var quotation: UITextView!
    @IBOutlet weak var person: UILabel!
    
    var quotations : [String] = ["The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart.", "Perfection is not attainable, but if we chase perfection we can catch excellence", "Try to be a rainbow in someone's cloud.", "Nothing is impossible, the word itself says \"I'm possible\"!"]
    
    var people : [String] = ["Helen Keller", "Vince Lombardi", "Maya Angelou", "Audrey Hepburn"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let randomValue : Int = Int(arc4random_uniform(UInt32(quotations.count)))
        let color = UIColor(red: 151/255, green: 237/255, blue: 253/255, alpha: 1)
        
        quotation.textAlignment = NSTextAlignment.Center
        quotation.text = quotations[randomValue]
        quotation.font = UIFont(name: "AvenirNext-regular", size: 26)
        quotation.backgroundColor = color
        person.text = "- \(people[randomValue])"
        person.font = UIFont(name: "AvenirNext", size: 18)
        
        self.view.backgroundColor = color
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
